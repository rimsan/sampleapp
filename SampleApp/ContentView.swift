//
//  ContentView.swift
//  SampleApp
//
//  Created by Roman on 30/06/2020.
//  Copyright © 2020 Rabobank. All rights reserved.
//

import SwiftUI



struct ContentView: View {
    
    @State var valueToggle: Bool = false
    
    private var valueBinding: Binding<String> { Binding<String> (
        get: { self.valueToggle ? "ToggleOn" : "ToggleOff" },
        set: { _ in }
        )
    }
    
    var body: some View {
        
        VStack {
            TextField("Value:", text: valueBinding).accessibility(identifier: "TextField")
            
            Button(action: {
                self.valueToggle = !self.valueToggle
            }, label: {
                Text("Push")
            }).accessibility(identifier: "Button")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
